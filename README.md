# Excite

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server
Clone the repo and inside the project root folder run the following commands.
  Run `npm install` for installing project dependencies. 
  Run `npm run start` for a dev server. 

Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
The node.js server is listening on port 3000.
