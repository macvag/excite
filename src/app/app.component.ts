import {
  Component,
  Renderer2,
  ViewChild,
  AfterViewInit,
  ElementRef
} from '@angular/core';

import { ApiService } from './services/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('screenValue') screen: ElementRef;
  openModal = false;

  numpad = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', 'delete'];
  value = '0';
  message: any;
  constructor(private renderer: Renderer2, private api: ApiService) { }

  ngAfterViewInit() {
    const initialText = this.renderer.createText(this.value);
    this.renderer.appendChild(this.screen.nativeElement, initialText);
  }

  calcValue(input: any) {
    if (input !== 'delete') {
      if (this.value !== '0') {
        this.value += input;
      } else {
        this.value = input;
      }
    } else {
      this.value = this.value.slice(0, -1) ? this.value.slice(0, -1) : '0';
    }
    this.screen.nativeElement.innerHTML = parseInt(this.value).toLocaleString();
  }

  withdrawMoney() {
    if (this.value === '0') {
      this.openModal = true;
      this.message = {
        title: 'Error',
        body: 'Please specify amount'
      };
    } else {
      const amount = { amount: parseInt(this.value) };
      this.api.withdrawAmount(amount).subscribe(
        (res: any) => {
          this.openModal = true;
          if (!res.error) {
            this.message = {
              title: 'Success',
              body: res
            };
          } else {
            this.message = {
              title: 'Error',
              body: res.error
            };
          }
        },
        error => console.log(error)
      );
    }
  }

  closeModal() {
    this.openModal = false;
  }
}
