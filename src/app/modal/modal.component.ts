import { Component, OnInit, OnChanges, Input, ViewChild, Renderer2, ElementRef, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnChanges {
  @ViewChild('modal') modal: ElementRef;
  @Input() message:any;
  @Output() close = new EventEmitter();
  success = true;
  banknotes = [];
  constructor(private renderer: Renderer2) { }

  ngOnInit() {
  }

  ngOnChanges() {
    console.log(this.message);
    if (this.message.title === 'Success') {
      this.success = true;
      this.message.body.map(item => {
        const key = Object.keys(item)[0];
        if (item[key] !== 0 ) {
          this.banknotes.push(key + 'x' + item[key]);
        }
      });
      console.log(this.banknotes);
    } else {
      this.success = false;
    }
  }

  closeModal() {
    this.close.emit('close');
  }

}
