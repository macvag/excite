import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  withdrawAmount(amount: any) {
    return this.http.post("/api/v1/withdraw/custom", amount);
  }
}
