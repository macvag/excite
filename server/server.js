var express = require('express');
var bodyParser = require('body-parser');
var request = require('request');

var app = express();
var router = express.Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

router.post('/', async (req, res, next) => {
    const body = req.body;

    try {
        request = request.defaults({
            followAllRedirects: true
        });

        request.post(
            'https://us-central1-atm-backend-2cc1b.cloudfunctions.net/withdr', body,
            function (error, response, body) {
                return res.json({ api: body });
            }
        );

    }
    catch (err) {
        // unexpected error
        return next(err);
    }
});

router.post('/custom', async (req, res, next) => {
  const amount = req.body.amount;
  const banknotes = [1000, 500, 100];
  const resBank = []
  calculateBanknotes(amount, banknotes, resBank);
  let validateResult = 0;
  resBank.map((item, i) => {
    const key = Object.keys(resBank[i])[0];
    validateResult += parseInt(key) * item[key]
  });

  if (amount === validateResult) {
    res.status(200).send(resBank)
  } else {
    res.status(200).send({error: "Invalid Amount"});
  }

});

app.use('/api/v1/withdraw', router);

var port = 4002;

app.listen(port, () => console.log(`Server listening on port ${port}!`))


const test = 1750;

function calculateBanknotes(test, banknotes, resBank) {
  for (var i = 0; i < banknotes.length; i++) {
    const quotient = Math.floor(test/banknotes[i]);
    const obj ={};
    obj[banknotes[i]] = quotient;
    resBank.push(obj);
    const remainder = test%banknotes[i];

    if(remainder !== 0) {
      calculateBanknotes(remainder, banknotes.splice(1), resBank);
    } else {
      return resBank;
    }
  }
  return resBank;
}
